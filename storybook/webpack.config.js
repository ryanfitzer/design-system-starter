const path = require( 'path' );

const root = path.resolve( __dirname, '../' );

module.exports = async ( { config } ) => {

    // https://webpack.js.org/configuration/resolve/#resolvealias
    config.resolve.alias = {

        ...config.resolve.alias,

        '@mdx-js': path.resolve( __dirname, 'node_modules/@mdx-js' ),
        '@storybook': path.resolve( __dirname, 'node_modules/@storybook' ),
        // https://remysharp.com/2019/11/04/nice-imports-with-nextjs
        '~': path.resolve( __dirname, root )
    };

    // Remove conflicting css rule
    // https://github.com/storybookjs/storybook/issues/6319#issuecomment-477852640
    config.module.rules = config.module.rules.filter( ( rule ) => rule.test.toString() !== /\.css$/.toString() );

    // Add root directory to the default jsx rule
    // https://github.com/storybookjs/storybook/issues/8162
    config.module.rules = config.module.rules.map( ( rule ) => {

        const isJSXRule = rule.test.toString() === /\.(mjs|jsx?)$/.toString();

        if ( isJSXRule ) {

            rule.include = [ root ];
            rule.exclude.push( /node_modules/ );

        }

        return rule;

    } );

    config.module.rules = [

        ...config.module.rules,

        {
            test: /\.css$/,
            use: [
                // https://github.com/webpack-contrib/style-loader#readme
                'style-loader',
                {
                    // https://github.com/webpack-contrib/css-loader#readme
                    loader: 'css-loader',
                    options: {
                        importLoaders: 1
                    }
                },
                {
                    // https://github.com/postcss/postcss-loader#readme
                    loader: 'postcss-loader',
                    options: {
                        config: {
                            path: path.resolve( __dirname, root )
                        }
                    }
                }
            ]
        }

    ];

    return config;

};