import { configure, addParameters } from '@storybook/react';
import theme from './theme';

// https://github.com/storybookjs/storybook/blob/next/addons/docs/README.md

const params = {
    // https://github.com/storybookjs/storybook/blob/master/docs/src/pages/configurations/options-parameter/index.md
    options: {
        theme: theme,
        panelPosition: 'right',
        sidebarAnimations: false
        /*
        // https://github.com/storybookjs/storybook/blob/master/lib/ui/README.md#story-order
        storySort: ( a, b ) => a[ 1 ].id.localeCompare( b[ 1 ].id )
        */
    }
};

const loaderFn = () => {

    const stories = [
        require( '~/docs/introduction.story.mdx' )
    ];

    // https://webpack.js.org/guides/dependency-management/#requirecontext
    const files = [
        require.context( '~/docs', true, /\.?(stories|story)\.mdx$/ ),
        require.context( '~/docs', true, /\.?(stories|story)\.js$/ ),
        require.context( '~/components', true, /\.?(stories|story)\.mdx$/ ),
        require.context( '~/components', true, /\.?(stories|story)\.js$/ )
    ];

    files.forEach( ( context ) => (
        context.keys().forEach( ( file ) => stories.push( context( file ) ) )
    ) );

    return stories;

};

addParameters( params );
configure( loaderFn, module );