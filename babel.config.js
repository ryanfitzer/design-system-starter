// https://babeljs.io/docs/en/options
module.exports = {

    // https://babeljs.io/docs/en/options#presets
    presets: [

        // https://babeljs.io/docs/en/babel-preset-env
        '@babel/preset-env',

        // https://babeljs.io/docs/en/babel-preset-react
        '@babel/preset-react'
    ]
};