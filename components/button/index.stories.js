import React from 'react';
import Button from './index';

export default {
    title: 'Button',
    component: Button
};

export const base = () => <Button text='Button One' />;