import './styles/index.css';
import React from 'react';
import { string, func } from 'prop-types';

/**
 * Just a plain `<button type="button" />`
 */
const Button = ( { text, onClick } ) => {

    return (
        <button
            type='button'
            className='button'
            onClick={ onClick }
        >
            { text }
        </button>
    );

};

Button.propTypes = {
    /** The text to show in the button. */
    text: string.isRequired,
    /** A handler function for when the button is clicked. */
    onClick: func
};

export default Button;