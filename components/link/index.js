import './styles/index.css';
import React from 'react';
import { string } from 'prop-types';

/**
 * Creates a link element via `<a href />`
 */
const Link = ( { text, href } ) => {

    return (
        <a className='link' href={ href }>{ text }</a>
    );

};

Link.propTypes = {
    /** Link destination */
    href: string.isRequired,
    /** The text to display */
    text: string.isRequired
};

export default Link;