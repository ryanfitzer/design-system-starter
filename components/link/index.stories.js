import React from 'react';
import Link from './index';

export default {
    title: 'Link',
    component: Link
};

export const base = () => <Link href='#' text='Display Text' />;