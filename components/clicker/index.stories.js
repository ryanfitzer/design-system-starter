import React from 'react';
import Clicker from './index';

export default {
    title: 'Clicker',
    component: Clicker
};

export const base = () => <Clicker />;