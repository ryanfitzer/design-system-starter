/*
 * Creates a file for each [theme].css file that includes all components.
 * PostCSS Runner: https://github.com/postcss/postcss/blob/master/docs/guidelines/runner.md
 */
const path = require( 'path' );
const { writeFileSync } = require( 'fs' );
const del = require( 'del' );
const glob = require( 'glob' );
const postcss = require( 'postcss' );
const makeDir = require( 'make-dir' );
const { bold, blue, blueBright, yellow, red } = require( 'colorette' );
const postcssrc = require( 'postcss-load-config' );
const { banner } = require( './banner' );

// Resolve directory paths
const root = path.resolve( __dirname, '../' );
const distDir = path.resolve( root, 'dist/styles' );
const themesDir = path.resolve( root, 'library/styles/themes' );
const componentsDir = path.resolve( root, 'components' );

// Create an array of component `@import` rules.
const components = glob.sync( `${ componentsDir }/**/index.css` ).map( ( filepath ) => {

    return `@import '${ path.relative( componentsDir, filepath ) }';`;

} );

// Create theme configs
let themes = glob.sync( `${ themesDir }/*.css` ).map( ( filepath ) => {

    const parsed = path.parse( filepath );

    const css = [
        banner,
        `@import '${ path.relative( componentsDir, filepath ) }';`,
        ...components
    ].join( '\n' );

    return {
        css,
        from: `${ componentsDir }/index.css`,
        to: `${ distDir }/${ parsed.name }.css`
    };

} );

// Create a default entry point if there's no default theme.
if ( !themes ) {

    themes = [ {
        from: `${ componentsDir }/index.css`,
        to: `${ distDir }/index.css`,
        css: [
            banner,
            ...components
        ].join( '\n' )
    } ];

}

// Load postcss.config.js
const config = postcssrc( {
    options: {
        defaultTheme: null
    }
} );

// Define PostCSS build runner
const build = ( { from, to, css } ) => {

    const relFrom = path.relative( root, from );
    const relTo = path.relative( root, to );

    config.then( ( { plugins, options } ) => {

        postcss( plugins )
          .process( css, Object.assign( options, { from, to } ) )
          .then( ( result ) => {

              console.info( `${ blueBright( relFrom ) } ${ blue( '→' ) } ${ blueBright( `${ relTo }...` ) }` );

              result.warnings().forEach( ( warn ) => {

                  console.info( yellow( `(!) ${ warn.toString() }` ) );

              } );

              writeFileSync( to, result.css );

              if ( result.map ) {

                  writeFileSync( `${ to }.map`, result.map.toString() );

              }

              console.info( `${ bold( blue( 'created' ) ) } ${ blueBright( relTo ) }\n` );

          } )
          .catch( ( error ) => {

              if ( error.name === 'CssSyntaxError' ) {

                  process.stderr.write( error.message + error.showSourceCode() );

                  console.info( red( `${ error.message }\n${ error.showSourceCode() }` ) );

              }
              else {

                  throw error;

              }

          } );

    } );

};

// Clear and create the `dist/styles` directory.
( async () => {

    await del( distDir );
    await makeDir( distDir );

    console.info( `cleared: ${ distDir }\n` );

} )();

// Build each theme.
themes.forEach( build );