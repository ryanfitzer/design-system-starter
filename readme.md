# About #

A baseline architecture for creating and distributing a React component library.

Uses Storybook for developing and documenting the components ([demo](https://ryanfitzer.gitlab.io/design-system-starter)).


## Development ##


### Install Dependencies ###

Install root project and Storybook dependencies:

```
npm install
npm run sb:install
```


### Start Storybook ###

```
npm run sb
```

Navigate to <http://localhost:4000> to view the component documentation.


### Themes ###

Themes can be defined in `library/styles/themes/[name].css`. By default, Storybook uses `library/styles/themes/index.css`. This can be changed by altering the `defaultTheme` in `postcss.config.js`.


## Distribution ##

This process is automatically run on `npm publish` via the `prepublishOnly` lifecycle hook.

```
npm run dist
```

Creates the following directory: 

```
└── dist
    ├── cjs
    ├── esm
    ├── umd
    └── styles
```

Each module format directory contains an `index.js`. The `esm` and `cjs` directories also contain individual exports for each component.

The `styles` directory contains an `index.css` and any other `[theme].css` files.



## Consuming as a Package ##

```
npm install design-system-starter
```

Using `import`:

```
import { Example } from 'design-system-starter';
```

Using `require`:

```
const { Example } = require( 'design-system-starter' );
```

The `main` entry point (in `package.json`) defaults to [UMD (Universal Module Definition)](https://github.com/umdjs/umd#readme).

Each component is also exposed in both [ECMAScript Module](https://nodejs.org/api/esm.html) and CommonJS formats.

### CSS ###

The CSS for all components is located at `node_modules/design-system-starter/dist/styles/index.css` and can be added using any approach supported by your application.


### CJS ###

Destructured from full library of components:

```
const { Example } = require( 'design-system-starter/dist/cjs' );
```

Individual component:

```
const Example = require( 'design-system-starter/dist/cjs/example' );
```


### ESM ##

Destructured from full library of components:

```
import { Example } from 'design-system-starter/dist/esm';
```

Individual component:

```
import Example from 'design-system-starter/dist/esm/example';
```


### UMD Browser Global ###

The [UMD (Universal Module Definition)](https://github.com/umdjs/umd#readme) format enables access to the components in both Node and the browser.

Components are exposed via the `DS` global variable in the browser.

```
<script src="path/to/dist/umd/index.js"></script>
<script>
  ReactDOM.render(
    React.createElement( DS.Example ),
    document.getElementById('root')
  );
</script>
```

Please see the React documentation for more information: [Add React to a Website](https://reactjs.org/docs/add-react-to-a-website.html).
